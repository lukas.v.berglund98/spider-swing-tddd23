﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour
{
    public bool triggered = false;

    public void OnTriggerEnter()
    {
        triggered = true;
    }
}
