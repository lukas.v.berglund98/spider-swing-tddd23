﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundmanager : MonoBehaviour
{

    public AudioSource sfxSrc;
    public AudioSource musicSrc;

    public static Soundmanager instance = null;

    public float lowPitch = .95f;
    public float highPitch = 1.05f;
    public float volume {get; set;}
    public bool mute {get; set;}

    // Start is called before the first frame update
    void Start()
    {
        mute = false;
        volume = .75f;
    }


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy (gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        sfxSrc.mute = mute;
        sfxSrc.volume = volume;
        musicSrc.mute = sfxSrc.mute;
        musicSrc.volume = sfxSrc.volume;
    }

    public void playSingle (AudioClip clip)
    {
            sfxSrc.clip = clip;
            sfxSrc.Play();
    }


    public void RandomizeSfx (params AudioClip[] clips)
    {   
            int randIndex = Random.Range(0, clips.Length);
            float randomPitch = Random.Range(lowPitch, highPitch);

            sfxSrc.pitch = randomPitch;
            sfxSrc.clip = clips[randIndex];
            sfxSrc.Play();
    } 

}
