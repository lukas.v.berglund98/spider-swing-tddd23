﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BoxMovement : MonoBehaviour
{
    public new Transform transform;
    public float speed = 3f;
    public Vector3 targetPos;
    private Vector3 prevPos;
    private Vector3 tempPos;

    void Start()
    {
        transform = GetComponent<Transform>();
        prevPos = transform.position;
    }

    void Update()
    {
        float step =  speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, targetPos, step);

        if (Vector3.Distance(transform.position, targetPos) < 0.001f)
        {
            tempPos = targetPos;
            targetPos = prevPos;
            prevPos = tempPos;
        }
    }
}
