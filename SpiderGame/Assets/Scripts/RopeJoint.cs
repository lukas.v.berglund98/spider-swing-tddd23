﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeJoint : MonoBehaviour
{
    
    public Rigidbody leftRb;
    public Rigidbody rightRb;
    private HingeJoint leftJoint;
    public HingeJoint rightJoint;
    public GameObject sphere;

    // Start is called before the first frame update
    void Start()
    {
        leftJoint = sphere.AddComponent<HingeJoint>();

        leftJoint.connectedBody = leftRb;
        rightJoint.connectedBody = rightRb;

        leftJoint.axis = Vector3.forward;
        leftJoint.anchor = new Vector3(0, 0, 0);
        rightJoint.axis = Vector3.forward;
        rightJoint.anchor = new Vector3(0, 0, 0);

        leftJoint.useLimits = rightJoint.useLimits;
        leftJoint.limits = rightJoint.limits;
        
        leftJoint.useSpring = rightJoint.useSpring;
        leftJoint.spring = rightJoint.spring;
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
