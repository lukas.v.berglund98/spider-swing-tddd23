﻿using UnityEngine;

public class FireTrigger : MonoBehaviour
{
    public GameManager gameManager;
    //
    //MOVE TO GAMEMANAGER!
    //

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            PlayerPrefs.SetInt("FireTrigger", 1);
            gameManager.EndGame();
        }
    }
}