﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grounded : MonoBehaviour
{
    public bool on_ground = false;

   // private Collider coll;

    // Start is called before the first frame update
    void Start()
    {
        //coll = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
       // if (coll.isTrig)
        //on_ground = false;
    }

    void OnTriggerStay(Collider col){
        //if(col.contacts[0].normal.y >= -100f){
            on_ground = true;
        //}
    }
    
    void OnTriggerExit(Collider col){
        on_ground = false;
    }
}
