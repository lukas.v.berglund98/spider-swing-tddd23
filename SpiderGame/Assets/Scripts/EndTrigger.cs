﻿using UnityEngine;

public class EndTrigger : MonoBehaviour
{
    public GameManager gameManager;
    //
    //MOVE TO GAMEMANAGER!
    //

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            gameManager.LevelComplete();
        }
    }
}

