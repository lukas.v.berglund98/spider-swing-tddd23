﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{
    
    public float speed = 1.5f;
    public float gravity = 10f;
    public float rot = 0f;
    public float rotSpeed = 100f;
    public float jumpForce = 0.5f;
    public float animSpeed = 1.5f;
    public Rigidbody rope; //Gameobject;
    public GameObject ropeObject;
    private bool grounded = false;
    private float tmpGravity = 0f;
    private float DistToGnd = 0f;
    private float fallTimer = 0f;
    public float maxFallTime = 1f;
    public bool ropePickedUp = false;
    private bool playerDead = false;
    Vector3 moveDir = Vector3.zero;
    CharacterController controller;
    Animator animator;
    Rigidbody rb;
    SpringJoint springJoint;
    CableComponent cableComponent;


    public Soundmanager Soundmanager;
    public AudioClip[] jumpSound;
    public AudioClip deathSound;
    public AudioClip[] fallDeathSound;

    public AudioClip ropeSound;


    // Start is called before the first frame update
    void Start()
    {
        DistToGnd = GetComponent<Collider>().bounds.extents.y;
        PlayerPrefs.SetInt("FireTrigger", 0);

        Debug.Log("Game start");
        controller = GetComponent<CharacterController> ();
        animator = GetComponent<Animator> ();
        rb = GetComponent <Rigidbody>();
        springJoint = GetComponent<SpringJoint>();
        cableComponent = GetComponent<CableComponent>();
        animator.SetFloat("Speed", animSpeed);
        FindObjectOfType<GameManager>().IntroText();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Movment();
        RopeHandler();
        PlayerFallCheck();

        int tempFire = PlayerPrefs.GetInt("FireTrigger");
        if (tempFire == 1)
        {
            playerDead = true;
            Soundmanager.instance.RandomizeSfx(fallDeathSound);
        }

        grounded = false;
    }

    void RopeHandler()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (rope.gameObject.GetComponentInChildren<Grounded>().on_ground)
            {
                rope.isKinematic = true;
            }
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            rope.isKinematic = false;
        }


        if (Input.GetKey(KeyCode.R))
        {
            if (springJoint.maxDistance <= 10f)
            {
                springJoint.maxDistance += 0.05f;
            }
        }
        else if (Input.GetKey(KeyCode.F))
        {
            if (springJoint.maxDistance > springJoint.minDistance)
            {
                springJoint.maxDistance -= 0.05f;
            }
        }

        if (springJoint.maxDistance <= (springJoint.minDistance + 0.3))
        {
            rope.drag = 0;
            rope.mass = 0;
            rope.angularDrag = 0;
            ropePickedUp = true;
        }
        else
        {
            rope.drag = 0.3f;
            rope.mass = 0.2f;
            rope.angularDrag = 0.05f;
            ropePickedUp = false;
        }
    }

    void PlayerFallCheck()
    {
        Debug.Log(fallTimer);
        if (fallTimer >= maxFallTime)
        {
            if (fallTimer >= maxFallTime+1.2)
            {
                Debug.Log("Fell out of the world");
                //  
                playerDead = true;
                FindObjectOfType<GameManager>().EndGame();
                Soundmanager.instance.RandomizeSfx(fallDeathSound);
            }
            else if (grounded)
            {
                Debug.Log("To hard jump");
                //  
                playerDead = true;
                FindObjectOfType<GameManager>().EndGame();
                Soundmanager.instance.RandomizeSfx(deathSound);
            }
        }

        if (grounded)
        {
            fallTimer = 0f;
        }
        else
        {
            if (!(rope.isKinematic || rope.gameObject.GetComponentInChildren<Grounded>().on_ground))
            {
                fallTimer += Time.deltaTime;
            }
            else if(ropePickedUp)
            {
                fallTimer += Time.deltaTime;
                Debug.Log(ropePickedUp);
            }
        }

    }

    void Movment()
    {

        if (!playerDead)
        {
            //moveDir = Vector3.zero;
            float rotDir = 1;


            if (Input.GetKey(KeyCode.W))
            {
                animator.SetInteger("Moving", 1);
                moveDir = new Vector3(0, 0, 1);
                moveDir *= speed;
                moveDir = transform.TransformDirection(moveDir);
                //Soundmanager.instance.RandomizeSfx(moveSound);


            }
            else if (Input.GetKey(KeyCode.S))
            {
                animator.SetInteger("Moving", -1);
                moveDir = new Vector3(0, 0, -1);
                moveDir *= speed;
                moveDir = transform.TransformDirection(moveDir);
                //Soundmanager.instance.RandomizeSfx(moveSound);
            }
            else
            {
                animator.SetInteger("Moving", 0);
                moveDir = Vector3.zero;
            }

            if (grounded)
            {

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    //moveDir += new Vector3 (0, jumpForce, 0);
                    rb.velocity += new Vector3(0, jumpForce, 0);
                    animator.SetInteger("Moving", 2);
                    Soundmanager.instance.RandomizeSfx(jumpSound);
                }

            }
            else
            {
                //moveDir.y = 0;  
                rb.AddForce(new Vector3(0, -gravity, 0), ForceMode.Force);
            }

            if (Input.GetKey(KeyCode.S))
            {
                rotDir = -1;
            }

            rot += Input.GetAxis("Horizontal") * rotSpeed * rotDir * Time.deltaTime;
            transform.eulerAngles = new Vector3(0, rot, 0);

            rb.AddForce(moveDir, ForceMode.Impulse);

            //controller.Move(moveDir * Time.deltaTime);
        }
        else
        {
            moveDir = Vector3.zero;
            rb.AddForce(moveDir, ForceMode.Impulse);
            transform.eulerAngles = new Vector3(0, rot, 0);
            rb.AddForce(new Vector3(0, -gravity, 0), ForceMode.Force);

            animator.SetInteger("Moving", 0);
            animator.SetInteger("Moving", 3);

            PlayerPrefs.SetInt("FireTrigger", 0);
        }
    }

    void OnCollisionStay(Collision col){
        if(col.contacts[0].normal.y >= 0.01f){
            grounded = true;
        }
    }

}

