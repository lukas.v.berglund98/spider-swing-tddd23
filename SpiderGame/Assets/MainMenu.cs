﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Button btn2, btn3, btn4, btn5, btn6;
    int level;

    private void Start()
    {
        btn2.interactable = false;
        btn3.interactable = false;
        btn4.interactable = false;
        btn5.interactable = false;
        btn6.interactable = false;

        int temp = PlayerPrefs.GetInt("first");

        if(temp != 1)
        {
            PlayerPrefs.SetInt("first", 1);
            PlayerPrefs.SetInt("level", 0);
        }

        level = PlayerPrefs.GetInt("level");

        if (level >= 6)
        {
            level = 5;
            PlayerPrefs.SetInt("level", 5);
        }

        switch (level)
        {
            case 1:
                btn2.interactable = true;
                break;
            case 2:
                btn2.interactable = true;
                btn3.interactable = true;
                break;
            case 3:
                btn2.interactable = true;
                btn3.interactable = true;
                btn4.interactable = true;
                break;
            case 4:
                btn2.interactable = true;
                btn3.interactable = true;
                btn4.interactable = true;
                btn5.interactable = true;
                break;
            case 5:
                btn2.interactable = true;
                btn3.interactable = true;
                btn4.interactable = true;
                btn5.interactable = true;
                btn6.interactable = true;
                break;
        }
    }

    public void ResetLevels()
    {
        btn2.interactable = false;
        btn3.interactable = false;
        btn4.interactable = false;
        btn5.interactable = false;
        btn6.interactable = false;
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("first", 1);
    }

    public void LevelToLoad(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void PlayGame()
    {
        //SceneManager.GetActiveScene().buildIndex + 1            

    }

    public void QuitGame()
    {
        Debug.Log("Quit game");
        Application.Quit();
    }
}
