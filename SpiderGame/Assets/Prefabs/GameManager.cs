﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public bool gameHasEnded = false;
    public float restartDelay = 2f;
    public int introDelay = 5;
    public GameObject completeLevelUI;
    public GameObject introLevelUI;
    public GameObject pauseMenuUI;
    public GameObject diedUI;

    private bool gameIsPaused = false;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        gameIsPaused = false;
        SceneManager.LoadScene("startMenu");
    }

    public void LevelComplete()
    {
        Debug.Log("Level won!");
        completeLevelUI.SetActive(true);

        int tempLevel = PlayerPrefs.GetInt("level");
        if (tempLevel <= SceneManager.GetActiveScene().buildIndex)
        {
            PlayerPrefs.SetInt("level", SceneManager.GetActiveScene().buildIndex);
        }
        Debug.Log("tempLevel: " + tempLevel + " Scene: " + SceneManager.GetActiveScene().buildIndex);
        Invoke("NextLevel", 2);
    }

    public void EndGame()
    {
        if(!gameHasEnded)
        {
            
            diedUI.SetActive(true);
            gameHasEnded = true;
            Invoke("Restart", restartDelay);
        }
        
    }


    public void IntroText()
    {
        //Debug.Log("IntroText");
        //StartCoroutine(ShowMessage(introDelay));
    }

    IEnumerator ShowMessage(float delay)
    {
        //Debug.Log("Start");
        introLevelUI.SetActive(true);
        yield return new WaitForSeconds(delay);
        introLevelUI.SetActive(false);
        //Debug.Log("Stop");
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
